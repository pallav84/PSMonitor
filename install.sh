sudo apt-get update

# Install the prerequisite software and repositories:

sudo apt-get install -y software-properties-common
sudo apt-get install -y python-software-properties
sudo apt-add-repository ppa:ansible/ansible

# Update the repository cache after adding a new repository
sudo apt-get update

# Install Ansible
sudo apt-get install -y ansible
