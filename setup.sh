#!/bin/bash

ping_hosts() {
  echo "Check hosts reachability"
  ansible -i inventory all -m ping
}

deploy_psmonitor() {
  echo "Deploying PSMonitor"
  ansible-playbook -i inventory playbooks/deploy_psmonitor.yml
}

  case $1 in
    ping_hosts) ping_hosts
    ;;
    deploy_psmonitor) deploy_psmonitor
    ;;
    *)
    echo "        ./$(basename $0) ping_hosts /  deploy_psmonitor /"
    ;;
  esac
